exports.up = function (knex) {
   return Promise.all([

   knex.schema
  
    //Registration
    .createTable("Registration",function(table){
       table.increments('id').primary();   
       table.string('userid');   
       table.string('orderid');         
       table.string('yblrefno',50);       
       table.string('virtualaddress',255);
       table.string('status',2);
       table.string('statusdesc');
       table.string('registrationdate',20);
       table.string('add1',50);
       table.string('add2',50);
       table.string('add3',50);
       table.string('add4',50);
       table.string('add5',50);
       table.string('add6',50);
       table.string('add7',50);
       table.string('add8',50);
       table.string('add9',50);
       table.string('add10',50);
    })
    //Transaction
    .createTable('Transaction',function(table){
       table.increments('id').primary();
       table.string('userid');
       table.string('ybltxnid',30); 
       table.string('merchanttxnid',50); 
       table.decimal('txnamount',16,2);
       table.timestamp('transauthdata').defaultTo(knex.fn.now());
       table.string('status',2);
       table.string('transtype',35);
       table.string('statusdesc');
       table.string('responsecode',5); 
       table.string('approvalnum',30);
       table.string('payerva',255);
       table.string('npcitxnid',35);
       table.string('temprefid',35);
       table.string('payeraccno',100);
       table.string('payerifsc',100);
       table.string('payeraccname',100);
       table.string('add1',100);
       table.string('add2',100);
       table.string('add3',100);
       table.string('add4',100);
       table.string('add5',100);
       table.string('add6',100);
       table.string('add7',100);
       table.string('add8',100);
       table.string('add9',100);
       table.string('add10',100);
    })
    //VPA Table
    .createTable('VpaConnections', function (table) {
      table.increments('id').primary();       
      table.string('userid',50);   
      table.string('username',50);
      table.string('nickname',50);      
      table.string('virtualaddress',20);
      table.timestamp('created_at').defaultTo(knex.fn.now());
    })
  
    //Payment
    .createTable('Payment', function (table) {
      table.increments('id').primary();   
      table.string('userid');      
      table.string('merchanttxnid',20);
      table.string('ybltxnid',18);
      table.string('virtualaddress',18);
      table.string('status',10);
      table.string('statusdescription',100);
      table.string('registrationdata',20);
    })
    //Account Activity
    .createTable('AccountActivity',function(table){
       table.increments('id').primary();
       table.string('userid');     
       table.string('virtualaddress',255);
       table.string('status',1);
       table.string('add1',50);
       table.string('add2',50);
       table.string('add3',50);
       table.string('add4',50);
       table.string('add5',50);
       table.string('add6',50);
       table.string('add7',50);
       table.string('add8',50);
       table.string('add9',50);
       table.string('add10',50);
       table.string('statusdesc',512);
       table.string('yblrefno',18);
       table.string('merchanttxnid',20);
       
    })    
    
    //Fetch Profile
    .createTable('Profile',function(table){
       table.increments('id').primary();
       table.string('userid');     
       table.string('customerid');
       table.string('accountid');
       table.string('accountnumber',100);
       table.string('ifsccode',20);
       table.string('accountname',255);
       table.string('accounttype',50);
       table.integer('statuscode');
       table.integer('curstatuscode');
       table.string('bankname',200);
       table.string('defaccflag',10);
       table.string('bankcode',50);
       table.string('fvaddr',50);
       table.string('crdtype',20);
       table.integer('crdlength');
       table.string('dmobile',20);
       table.string('mpinstatus',1);
       table.string('atmcrdlength',1);
       table.string('status',1);
       table.integer('regrefid');
       table.string('add1',50);
       table.string('add2',50);
       table.string('add3',50);
       table.string('add4',50);
       table.string('add5',50);
       table.string('add6',50);
       table.string('add7',50);
       table.string('add8',50);
       table.string('add9',50);
       table.string('add10',50);
       table.string('statusdesc',512);
       table.string('yblrefid',18);
       table.string('yblrefno',18);
       table.string('accholdername',18);
       table.string('merchanttxnid',20);      
    })    
   //transaction 
   // //Transaction
       //[{pgMeTrnRefNo=190514, orderNo=5nqh84hiftjg0ioqo4lqspcm, status=S, payerVA=9988490320@yesb, payerIfsc=YESB0000220,
      //    add1=NA, add2=NA, add3=NA, add4=NA, add5=NA, add6=NA, add7=NA, add8=NA, add9=NA, add10=NA, 
      //refId=716420027472, approvalCode=NA, statusDesc=Transaction Collect request initiated successfully, 
      //txnAmount=1, payerAccName=NA, npciTxnId=YESB51D99237FA0244E7E05400144FFA530, responsecode=NA, 
      //payerAccountNo=NA, tranAuthdate=2017:06:13 08:23:15}]
    
    
  //  Bundle[{pgMeTrnRefNo=36p8sqahhipf1gpg5o9i5ktdr, orderNo=null, status=S, payerVA=8591752316@yesb, payerIfsc=8591752316, add1=4377, add2=NA, add3=NA, add4=NA, add5=NA, add6=NA, add7=null, add8=null, add9=null, add10=null, refId=NA, approvalCode=NA, statusDesc=Transaction success, txnAmount=1.00, yblTxnID=45608, payerAccName=YESB0000220, npciTxnId=YESB51E5BD505A594A30E05400144FF95A7, responsecode=00, custRefID=716510027475, payerAccountNo=NA, tranAuthdate=2017:06:14 10:56:25, merchantTxnId=null}]
     //collection Inbox after accepting transaction 
     .createTable('inbox',function(table){
       table.increments('id').primary();
       table.string('userid');     
       table.string('pgmetrnrefno',20); 
       table.string('orderno',100); 
       table.string('status',1); 
       table.string('payerva',50); 
       table.string('payerifsc',50);
       table.string('add1',50);
       table.string('add2',50);
       table.string('add3',50);
       table.string('add4',50);
       table.string('add5',50);
       table.string('add6',50);
       table.string('add7',50);
       table.string('add8',50);
       table.string('add9',50);
       table.string('add10',50);
       table.string('refid',20);
       table.string('approvalcode',2);
       table.string('statusdesc',512);
       table.decimal('txnamount',8,2);
       table.string('ybltxnid',20); 
       table.string('payeraccname',20);
       table.string('npcitxnid',100);
       //table.string('payeraccname',20); 
       table.string('responsecode',20);        
       table.string('custrefid',20); 
       table.string('payeraccountno',50);
       table.string('tranauthdate',100);       
       table.string('merchanttxnid',100);
     }) 
    
    //Log    
    .createTable('Log',function(table){
       table.increments('id').primary();
       table.string('userid');     
       table.string('level',16);
       table.string('message',512);
       table.string('meta',1024);
       table.timestamps();
    })

    //VPA
    .createTable('VpaService',function(table){
       table.increments('id').primary();
       table.string('userid');
       table.string('vpa',512);
       table.string('username',512);
       table.string('nickname',512);
       table.timestamps();
    })
   ])
};

exports.down = function (knex) {
   return Promise.all([
  knex.schema
    .dropTableIfExists('Payment')
    .dropTableIfExists('Registration')
    .dropTableIfExists('AccountActivity')
    .dropTableIfExists('Profile')
    .dropTableIfExists('Transaction')
    .dropTableIfExists('Inbox')
    .dropTableIfExists('Log')
    .dropTableIfExists('VpaService')
     .dropTableIfExists('VpaConnections')
   ])
};