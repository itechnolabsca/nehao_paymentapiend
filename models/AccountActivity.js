'use strict';

const Model = require('objection').Model;
class AccountActivity extends Model {

  static get tableName() {
    return 'AccountActivity';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['userid'],
      properties: {
        id: { type: 'integer' },
        userid: { type: 'string' },
        virtualaddress: { type: 'string' },
        status: { type: 'string' },
        add1: { type: 'string' },
        add2: { type: 'string' },
        add3: { type: 'string' },
        add4: { type: 'string' },
        add5: { type: 'string' },
        add6: { type: 'string' },
        add7: { type: 'string' },
        add8: { type: 'string' },
        add9: { type: 'string' },
        add10: { type: 'string' },
        statusdesc: { type: 'string' },
        yblrefno: { type: 'string' },
        merchanttxnid: { type: 'string' }

      }
    }
  }



}

module.exports = AccountActivity;