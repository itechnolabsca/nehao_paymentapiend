'use strict';

const Model = require('objection').Model;
class Profile extends Model {

  static get tableName() {
    return 'Profile';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['userid'],
      properties: {
        id: { type: 'integer' },
        userid: { type: 'string' },
        customerid: { type: 'string' },
        accountid: { type: 'string' },
        accountnumber: { type: 'string' },
        ifsccode: { type: 'string' },
        accountname: { type: 'string' },
        accounttype: { type: 'string' },
        statuscode: { type: 'string' },
        curstatuscode: { type: 'string' },
        bankname: { type: 'string' },
        defaccflag: { type: 'string' },
        bankcode: { type: 'string' },
        fvaddr: { type: 'string' },
        crdtype: { type: 'string' },
        crdlength: { type: 'string' },
        dmobile: { type: 'string' },
        mpinstatus: { type: 'string' },
        atmcrdlength: { type: 'string' },
        status: { type: 'string' },
        regrefid: { type: 'string' },
        add1: { type: 'string' },
        add2: { type: 'string' },
        add3: { type: 'string' },
        add4: { type: 'string' },
        add5: { type: 'string' },
        add6: { type: 'string' },
        add7: { type: 'string' },
        add8: { type: 'string' },
        add9: { type: 'string' },
        add10: { type: 'string' },
        statusdesc: { type: 'string' },
        yblrefid: { type: 'string' },
        yblrefno: { type: 'string' },
        accholdername: { type: 'string' },
        merchanttxnid: { type: 'string' }
      }
    }
  }



}

module.exports = Profile;