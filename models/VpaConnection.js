'use strict';

const Model = require('objection').Model;
class VpaConnections extends Model {

  static get tableName() {
    return 'VpaConnections';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['userid'],
      properties: {
        id: { type: 'integer' },
        userid: { type: 'string' },
        ybltxnid: { type: 'bigint' },
        merchanttxnid: { type: 'string' },
        txnamount: { string: 'decimal' },
        transauthdata: { type: 'timestamp' },
        status: { type: 'string' },
        statusdesc: { type: 'string' },
        responsecode: { type: 'string' },
        approvalnum: { type: 'string' },
        payerva: { type: 'string' },
        npcitxnid: { type: 'string' },
        temprefid: { type: 'string' },
        payeraccno: { type: 'string' },
        payerifsc: { type: 'string' },
        payeraccname: { type: 'string' },




        add1: { type: 'string' },
        add2: { type: 'string' },
        add3: { type: 'string' },
        add4: { type: 'string' },
        add5: { type: 'string' },
        add6: { type: 'string' },
        add7: { type: 'string' },
        add8: { type: 'string' },
        add9: { type: 'string' },
        add10: { type: 'string' },
        refid: { type: 'string' },
        approvalcode: { type: 'string' },

        payeraccountno: { type: 'string' },
        tranauthdate: { type: 'string' }
      }
    }
  }
}
module.exports = VpaConnections;