'use strict';

const Model = require('objection').Model;
class CollectionInbox extends Model {

  static get tableName() {
    return 'CollectionInbox';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['userid'],
      properties: {
        id: { type: 'integer' },
        userid: { type: 'string' },
        pgmetrnrefno: { type: 'string' },
        orderno: { type: 'string' },
        status: { type: 'string' },
        payerva: { type: 'string' },
        payerifsc: { type: 'string' },
        add1: { type: 'string' },
        add2: { type: 'string' },
        add3: { type: 'string' },
        add4: { type: 'string' },
        add5: { type: 'string' },
        add6: { type: 'string' },
        add7: { type: 'string' },
        add8: { type: 'string' },
        add9: { type: 'string' },
        add10: { type: 'string' },
        refid: { type: 'string' },
        approvalcode: { type: 'string' },
        statusdesc: { type: 'string' },
        txnamount: { type: 'decimal' },
        ybltxnid: { type: 'string' },
        payeraccname: { type: 'string' },
        npcitxnid: { type: 'string' },
        responsecode: { type: 'string' },
        custrefid: { type: 'string' },
        payeraccountno: { type: 'string' },
        tranauthdate: { type: 'string' },
        merchanttxnid: { type: 'string' }

      }
    }
  }



}

module.exports = CollectionInbox;