'use strict';

const Model = require('objection').Model;
class VpaService extends Model {

  static get tableName() {
    return 'VpaService';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['userid'],
      properties: {
        id: { type: 'integer' },
        userid: { type: 'string' },
        vpa: { type: 'string' },
        username: { type: 'string' },
        nickname: { type: 'string' }
      }
    }
  }



}

module.exports = VpaService;