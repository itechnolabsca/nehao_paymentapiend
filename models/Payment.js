'use strict';

const Model = require('objection').Model;
class Payment extends Model {
   
  static get tableName() {
    return 'Payment';
  }   
  static get jsonSchema() {
    
    return {
        type: 'object',
        required: ['userid'],
      properties: {
        id: {type: 'integer'},
        userid: {type: 'string'},
        merchanttxnid: {type: 'string'},
        ybltxnid: {type: 'string'},
        virtualaddress: {type: 'string'},
        status: {type: 'string'},
        statusdescription: {type: 'string'},
        registrationdata: {type: 'string'}
      }
    }
  }

  
 
}

module.exports = Payment;