'use strict';

const Model = require('objection').Model;
class Registration extends Model {

    static get tableName() {
        return 'Registration';
    }
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['userid'],
            properties: {
                id: { type: 'integer' },
                userid: { type: 'string' },
                orderid: { type: 'string' },
                yblrefno: { type: 'string' },
                virtualaddress: { type: 'string' },
                status: { type: 'string' },
                statusdesc: { type: 'string' },
                registrationdate: { type: 'string' },
                add1: { type: 'string' },
                add2: { type: 'string' },
                add3: { type: 'string' },
                add4: { type: 'string' },
                add5: { type: 'string' },
                add6: { type: 'string' },
                add7: { type: 'string' },
                add8: { type: 'string' },
                add9: { type: 'string' },
                add10: { type: 'string' },
               
            }
        }
    }



}

module.exports = Registration;