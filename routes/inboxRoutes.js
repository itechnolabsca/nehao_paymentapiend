'use strict';
let inboxCtrl = require("../controllers/inboxController");
module.exports = function (app) {
  app.route('/collectioninbox')
    .post(async (req, res) => {
      try {
        const userResult = await inboxCtrl.saveCollectionInbox(req.body);
        if (userResult.statusCode && userResult.statusCode != 200) {
          res.status(userResult.statusCode).send({ "message": userResult });
          return;
        }
        //DB Errors
        else if (userResult.errno) {
          res.status(400).send({ "message": userResult });
          return;
        }
        res.status(200).send({ "data": userResult });
      } catch (e) {
        res.status(500).send({ "error": e });
      }
    });

};
