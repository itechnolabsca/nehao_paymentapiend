'use strict';
var request = require("request");
let config = require("../config/app.config");
let registrationCtrl = require("../controllers/registrationController");
module.exports = function (app) {


    function doRequest(auth) {
        return new Promise(function (resolve, reject) {
            request({
                method: 'GET',
                headers: {
                    'authorization': auth
                },
                url: config.app.USER_VARIFY_URL,
                json: true
            }, function (error, response, body) {
                resolve(body);
            });
        });
    }

    app.route('/registration')
        .post(async (req, res) => {
            try {

                const userDetails = await doRequest(req.headers.authorization);
                if (userDetails && userDetails.statusCode == 200) {
                    req.body.userid = userDetails.data._id
                    const userResult = await registrationCtrl.saveRegistration(req.body);
                    if (userResult.statusCode && userResult.statusCode != 200) {
                        res.status(userResult.statusCode).send({"message": userResult});
                        return;
                    }
                    //DB Errors
                    else if (userResult.errno) {
                        res.status(400).send({"message": userResult});
                        return;
                    }
                    else {
                        let paymentProfileInfo = await registrationCtrl.getPaymentProfile(req.body);
                        let vpa = req.body.virtualaddress;
                        let yblrefno = req.body.yblrefno;

                        request({
                            method: 'POST',
                            headers: {
                                'authorization': req.headers.authorization
                            },
                            url: config.app.BACKEND_PAYMENT_URL, body: {'vpa': vpa, 'paymentProfileId': yblrefno},
                            json: true
                        }, function (error, response, body) {
                            if (error) {
                                res.status(400).send(error)
                            }
                            else {
                                res.status(200).send({"data": userResult})
                            }
                        });
                    }
                }
                else {
                    res.status(400).send({"message": userDetails.message});
                    return;
                }
            } catch (e) {
                res.status(500).send({"error": e});
            }
        })
}