'use strict';
let vpaCtrl = require("../controllers/vpaController");
module.exports = function (app) {


    function doRequest(auth) {
        return new Promise(function (resolve, reject) {
            request({
                method: 'GET',
                headers: {
                    'authorization': auth
                },
                url: config.app.USER_VARIFY_URL,
                json: true
            }, function (error, response, body) {
                resolve(body);
            });
        });
    }



    /**************POST VPA */
    app.route('/vpa')
        .post(async (req, res) => {
            try {
                const userDetails = await doRequest(req.headers.authorization)
                if (userDetails && userDetails.statusCode == 200) {
                    req.body.userid = userDetails.data._id
                    const userResult = await vpaCtrl.saveVpa(req.body)
                    if (userResult.statusCode && userResult.statusCode != 200) {
                        res.status(userResult.statusCode).send({"message": userResult})
                        return;
                    }
                    //DB Errors
                    else if (userResult.errno) {
                        res.status(400).send({"message": userResult})
                        return;
                    }
                    res.status(200).send({"data": userResult})
                }
                else {
                    res.status(400).send({"message": userDetails.message})
                    return;
                }
            } catch (e) {
                res.status(500).send({"error": e})
            }
        })



    /***************GET VPA********* */
    app.route('/vpa')
        .get(async function (req, res) {
            try {
                const userDetails = await doRequest(req.headers.authorization)
                if (userDetails && userDetails.statusCode == 200) {

                    const userResult = await vpaCtrl.getVpa(userDetails.data._id)
                    if (userResult.statusCode && userResult.statusCode != 200) {
                        res.status(userResult.statusCode).send({"message": userResult})
                        return;
                    }
                    //DB Errors
                    else if (userResult.errno) {
                        res.status(400).send({"message": userResult})
                        return;
                    }
                    else if (userResult == null) {
                        res.status(200).send({"data": "No data Exists for userid=" + "" + userDetails.data._id})
                    }
                    res.status(200).send({"data": userResult})
                }
                else {
                    res.status(400).send({"message": userDetails.message})
                    return;
                }
            } catch (e) {
                res.status(500).send({"error": e})
                return;
            }

        })
}
