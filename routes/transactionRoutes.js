'use strict';
const transIdLength = 12;
var request = require("request");
let transactionCtrl = require("../controllers/transactionController");
let config = require("../config/app.config");
module.exports = function (app) {


    function doRequest(auth) {
        return new Promise(function (resolve, reject) {
            request({
                method: 'GET',
                headers: {
                    'authorization': auth
                },
                url: config.app.USER_VARIFY_URL,
                json: true
            }, function (error, response, body) {
                resolve(body);
            });
        });
    }

    /*****************SAVE TRANSACTION***********/
    app.route('/transaction')
        .post(async (req, res) => {
            try {
                const userDetails = await doRequest(req.headers.authorization);
                if (userDetails && userDetails.statusCode == 200) {
                    req.body.userid = userDetails.data._id
                    const userResult = await transactionCtrl.saveTransaction(req.body);
                    if (!userResult) {
                        res.status(404).send({"message": "Unable to save transaction"});
                        return;
                    }
                    res.status(200).send({"data": userResult});
                }
                else {
                    res.status(400).send({"message": userDetails});
                    return;
                }
            } catch (e) {
                res.status(500).send({"error": e});
            }
        });


    /******************GET TRANSACTION ID ,MERCHANTID,MERCHANTKEY,APPNAME ************/
    app.route('/transaction')
        .get(async (req, res) => {
            try {
                const userDetails = await doRequest(req.headers.authorization);
                if (userDetails && userDetails.statusCode == 200) {

                    let transId = require("secure-random")(transIdLength, {type: 'Buffer'}).toString('hex');
                    var date = new Date().getDate()
                    var month = new Date().getMonth()
                    var year = new Date().getFullYear()
                    const userResult = date + "" + month + "" + year + "" + transId;
                    if (!userResult) {
                        res.status(404).send({"message": "Unable to save transaction"});
                        return;
                    }
                    res.status(200).send({
                        "TransactionId": userResult,
                        "merchant_id": config.app.MERCHANT_ID,
                        "merchant_key": config.app.MERCHANT_KEY,
                        "app_name": config.app.APP_NAME
                    });
                }
                else {
                    res.status(400).send({"message": userDetails});
                    return;
                }
            } catch (e) {
                res.status(500).send({"error": e});
            }
        });


    /*******TRANSACTION BY USERID */
    app.route('/allTransactions')
        .get(async function (req, res) {
            try {
                const userDetails = await doRequest(req.headers.authorization);
                if (userDetails && userDetails.statusCode == 200) {
                    const userResult = await transactionCtrl.getTransactionDetail(userDetails.data._id);
                    if (userResult.statusCode && userResult.statusCode != 200) {
                        res.status(userResult.statusCode).send({"message": userResult});
                        return;
                    }
                    //DB Errors
                    else if (userResult.errno) {
                        res.status(400).send({"message": userResult});
                        return;
                    }
                    else if (userResult == null) {
                        res.status(200).send({"data": "No data Exists for userid=" + "" + userDetails.data._id});
                    }
                    res.status(200).send({"data": userResult});
                }
                else {
                    res.status(400).send({"message": userDetails.message});
                    return;
                }
            } catch (e) {
                res.status(500).send({"error": e});
                return;
            }

        })
}
