'use strict';
let config = require("../config/app.config");
/**************API RETURN IFSC CODE By ID */
module.exports = function (app,rp) {
    app.route('/getIFSCbyId')
        .post(async (req, res) => {             
            var options = {
                method: 'POST',
                uri: config.app.IFSC_CODE_CHK_URL, body: { 'ifsc': req.body.ifsc},
                json: true
            };
            console.log(options);
            rp(options)
                .then(function (parsedBody) {
                    res.status(200).send({ "data": parsedBody });
                })
                .catch(function (err) {
                    res.status(500).send({ "err": "unable to post data to IFSC URL" });
                });

        });

};
