'use strict';

var request = require("request");
let config = require("../config/app.config");
let paymentCtrl = require("../controllers/paymentController");
let registrationCtrl = require("../controllers/registrationController");
module.exports = function (app, rp) {
    //Send UserId in body


    function doRequest(auth) {
        return new Promise(function (resolve, reject) {
            request({
                method: 'GET',
                headers: {
                    'authorization': auth
                },
                url: config.app.USER_VARIFY_URL,
                json: true
            }, function (error, response, body) {
                resolve(body);
            });
        });
    }


    app.route('/payment')
        .post(async (req, res) => {
            try {
                const userDetails = await doRequest(req.headers.authorization)
                if (userDetails && userDetails.statusCode == 200) {
                    req.body.userid = userDetails.data._id
                    const userResult = await paymentCtrl.createPayment(req.body)
                    if (userResult.statusCode && userResult.statusCode != 200) {
                        res.status(userResult.statusCode).send({"message": userResult})
                        return;
                    }
                    //DB Errors
                    else if (userResult.errno) {
                        res.status(400).send({"message": userResult});
                        return;
                    }
                    else {
                        let paymentProfileInfo = await registrationCtrl.getPaymentProfile(req.body)
                        let vpa = paymentProfileInfo[0].virtualaddress;
                        let yblrefno = paymentProfileInfo[0].yblrefno;
                        var options = {
                            method: 'POST',
                            headers: {
                                'authorization': 'bearer' + ' ' + req.get('accessToken')
                            },
                            // uri: config.app.BACKEND_PAYMENT_URL, body: {   'vpa': paymentProfileInfo.virtualaddress,'paymentProfileId': paymentProfileInfo.yblrefno, },
                            uri: config.app.BACKEND_PAYMENT_URL, body: {'vpa': vpa, 'paymentProfileId': yblrefno},
                            json: true
                        };
                        console.log(options);
                        rp(options)
                            .then(function (parsedBody) {
                                res.status(200).send({"data": parsedBody})
                            })
                            .catch(function (err) {
                                res.status(500).send({"err": "unable to post data to backend"})
                            });

                    }
                }
                else {
                    res.status(400).send({"message": userDetails.message})
                    return;
                }

            } catch (e) {
                res.status(500).send({"error": e})
            }
        })
}
