var express = require('express'),
    app = express(),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),   
  path = require('path'),
  port = process.env.PORT || 3002;
   var swaggerJSDoc = require('swagger-jsdoc');
var rp = require('request-promise');
var Model = require("objection").Model;
var  knexConfig = require('./knexfile');
var Knex = require("knex");
const knex = Knex(knexConfig.development);
Model.knex(knex);
// swagger definition


////////////////////Adding swagger ui to express///////////////////////
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

var showExplorer = false;
var options = {};
var customCss = '#header { display: none }';

app.use('/documentation', swaggerUi.serve, swaggerUi.setup(swaggerDocument, showExplorer, options, customCss));
//////////////////////////////////////////////////////////////////////



var swaggerDefinition = {
  info: {
    title: 'Payment swagger api',
    version: '1.0.0',
    description: 'Payment RESTful API with Swagger',
  },
  host: 'localhost:3002',
  basePath: '/',
};
/*********SWAGGER CONFIG*******/
// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.js'], 
};
// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
/*********CONFIGURE CUSTOM WINSTON LOGGER */ 
 
/********CONFIGURE ROUTES********** */
require('./routes/paymentRoutes')(app,rp);
require('./routes/registrationRoutes')(app);
require('./routes/accountActivityRoutes')(app);
require('./routes/profileRoutes')(app);
require('./routes/transactionRoutes')(app);
require('./routes/inboxRoutes')(app);
require('./routes/ifscRoutes')(app,rp);
require('./routes/vpaServiceRoutes')(app);

// serve swagger
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.listen(port);
console.log("Swagger listening on Url =>  http://localhost:3000/api-docs/" );
console.log('Payment API server started on Url =>  http://localhost:' + port);
