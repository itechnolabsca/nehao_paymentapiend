'use strict';
var paymentModel = require("../models/Payment");
 
/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.createPayment = async function (payload) {    
  
    try {
         let paymentData = await paymentModel
        .query()
        .insert(payload);
    return paymentData;
        
} catch (error) {
     return error;     
    }
   
}


