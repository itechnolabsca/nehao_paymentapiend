'use strict';
var vpaModel = require("../models/VpaConnection");

/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.saveVpa = async function (payload) {

    try {
        let vpaData = await vpaModel
            .query()
            .insert(payload);
        return vpaData;
    }
    catch (error) {
        return error;
    }
}
/***********GET TRANSACTION DETAIL BY USERID */
exports.getVpa = async function (userid) {
    if (!userid) return null;
    try {
        let vpaData = await vpaModel
            .query().select('username', 'nickname', 'virtualaddress')
            .where('userid', '=', userid);
        return vpaData;
    }
    catch (error) {
        return error;
    }
}
