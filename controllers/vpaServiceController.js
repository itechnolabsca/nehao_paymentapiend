'use strict';
var vpaService = require("../models/VpaService");

/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.saveVpaService = async function (payload) {

    try {
        let vpaServiceData = await vpaService
            .query()
            .insert(payload);
        return vpaServiceData;
    }
    catch (error) {
        return error;
    }
}
/***********GET VPA DETAIL BY USERID */
exports.getVpaService = async function (userid) {
    if (!userid) return null;
    try {
        let vpaServiceData = await vpaService
            .query().select('*')
            .where('userid', '=', userid);
        return vpaServiceData;
    }
    catch (error) {
        return error;
    }
}
