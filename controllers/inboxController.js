//saveCollectionInbox
'use strict';
var collectionInboxModel = require("../models/CollectionInbox");

/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.saveCollectionInbox = async function (payload) {

    try {
        let collectionData = await collectionInboxModel
            .query()
            .insert(payload);
        return collectionData;
    }
    catch (error) {
        return error;
    }
}