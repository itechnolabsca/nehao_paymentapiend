'use strict';
var registrationModel = require("../models/Registration");

/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.saveRegistration = async function (payload) {
    try {
        let registrationData = await registrationModel
            .query()
            .insert(payload);
        return registrationData;
    }
    catch (error) {
        return error;
    }
}


/*********************GET PAYMENT PROFILE DATA BASED ON USERID SUPPLIED********* */
exports.getPaymentProfile = async function (payload) {

    if (!payload.userid) return null;
    let registrationData = await registrationModel
        .query()
        .where("userid", "=", payload.userid);
    return registrationData;
}