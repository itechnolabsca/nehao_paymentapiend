//createAccountActivity
'use strict';
var accountActivityModel = require("../models/AccountActivity");

/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.createAccountActivity = async function (payload) {

    
    try {
        let activityData = await accountActivityModel
            .query()
            .insert(payload);
        return activityData;

    } catch (error) {
        return error;
    }

}