'use strict';
var transactionModel = require("../models/Transaction");

/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.saveTransaction = async function (payload) {

    try {
        let transactionData = await transactionModel
            .query()
            .insert(payload);
        return transactionData;
    }
    catch (error) {
        return error;
    }
}
/***********GET TRANSACTION DETAIL BY USERID */
exports.getTransactionDetail = async function (userid) {
    if (!userid) return null;
    try {
        let transactionData = await transactionModel
            .query().select('*')
            /*.query().select('ybltxnid',
                'merchanttxnid',
                'txnamount',
                'transauthdata',
                'status',
                'statusdesc',
                'responsecode',
                'approvalnum',
                'payerva',
                'npcitxnid',
                'temprefid',
                'payeraccno',
                'payerifsc',
                'payeraccname',
                'approvalcode',
                'payeraccountno',
                'tranauthdate')*/
            .where('userid', '=', userid);
        return transactionData;
    }
    catch (error) {
        return error;
    }
}
