'use strict';
var profileModel = require("../models/Profile");
 
/********************ACCEPT REST PAYLOAD and PERFORM INSERT */
exports.saveProfile = async function (payload) {
    
   
    let profileData = await profileModel
        .query()
        .insert(payload);
    return profileData;
}